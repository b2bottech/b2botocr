# README #

This is the repository for OCR algorithm used on TextZone detected in the first stage of the VPP

### What is this repository for? ###

* Quick summary: b2bot-tech/Dataset/b2botocr
* Version 1.0.0


### How do I get set up? ###

* Summary of set up:

    data directory includes:  
        weight of tesseract pretrained for french with Tesseract 4.0.0
    cfg directory includes: 
        NA

* Configuration
* Dependencies

    Pretrained data: https://github.com/tesseract-ocr/tessdata

    Information on Tesseract: https://github.com/tesseract-ocr/tesseract

    Licence is Apache v2.0 from: http://www.apache.org/licenses/LICENSE-2.0

    It use Leptonica Library under BSD-2 license from: http://www.leptonica.org/

* Database configuration
* How to run tests
* Deployment instructions
 
    Usage withing the VPP engine from: 
    https://github.com/ezdayo/vpp

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: francois.badaud@gmail.com
* Other community or team contact: ezdayo@gmail.com